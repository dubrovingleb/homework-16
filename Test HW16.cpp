﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>


const int N = 10;

int main()
{
	int A[N][N];
	std::cout << "Array " << N << " x " << N << ":" << '\n';

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			A[i][j] = i + j;
			std::cout << A[i][j] << ' ';
		}
		std::cout << '\n';
	}
	std::cout << '\n';

	int sum = 0; // для суммы элементов в строке
	int line; // нужная строка
	time_t t;
	time(&t);
	int day = localtime(&t)->tm_mday; // день месяца
	

	for (int x = 0; x < N; x++)
	{
		line = day % N;
		sum += A[line][x];
	}
	
	std::cout << "Sum of elements of " << line << " lines: " << sum << '\n';

	return 0;
}